package be.ucll.java.mobile.appusage;

import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class MainFragment extends Fragment {

    private static final String TAG = MainFragment.class.getSimpleName();

    private UsageStatsManager usageStatsManager;
    private MyAdapter myAdapter;
    private RecyclerView recyclerView;
    private Button btnOpenAppUsageSettings;
    private Spinner lstPeriod;

    public MainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        usageStatsManager = (UsageStatsManager) getActivity().getSystemService(Context.USAGE_STATS_SERVICE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View rootView, Bundle savedInstanceState) {
        super.onViewCreated(rootView, savedInstanceState);

        myAdapter = new MyAdapter();
        recyclerView = rootView.findViewById(R.id.recyclerview_apps);
        recyclerView.scrollToPosition(0);
        recyclerView.setAdapter(myAdapter);
        btnOpenAppUsageSettings = rootView.findViewById(R.id.btnOpenAppUsageSettings);
        lstPeriod = rootView.findViewById(R.id.lstPeriod);
        SpinnerAdapter spinnerAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.period_list, android.R.layout.simple_spinner_dropdown_item);
        lstPeriod.setAdapter(spinnerAdapter);
        lstPeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            String[] periods = getResources().getStringArray(R.array.period_list);

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StatsUsageInterval statsUsageInterval = StatsUsageInterval.getValue(periods[position]);
                if (statsUsageInterval != null) {
                    List<UsageStats> usageStatsList = getUsageStatistics(statsUsageInterval.interval);
                    Collections.sort(usageStatsList, new LastTimeLaunchedComparatorDesc());
                    updateAppsList(usageStatsList);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    /**
     * Returns the {@link #recyclerView} including the time span specified by the intervalType argument.
     *
     * @param intervalType The time interval by which the stats are aggregated.
     *                     Corresponding to the value of {@link UsageStatsManager}.
     *                     E.g. {@link UsageStatsManager#INTERVAL_DAILY}, {@link UsageStatsManager#INTERVAL_WEEKLY},
     * @return A list of {@link android.app.usage.UsageStats}.
     */
    public List<UsageStats> getUsageStatistics(int intervalType) {
        // Get the app statistics since one year ago from the current time.
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -1);

        List<UsageStats> queryUsageStats = usageStatsManager
                .queryUsageStats(intervalType, cal.getTimeInMillis(), System.currentTimeMillis());
        if (queryUsageStats.size() == 0) {
            Log.i(TAG, "The user did not allow access to apps usage!");
            Toast.makeText(getActivity(),
                    getString(R.string.explanation_access_to_appusage_is_not_enabled),
                    Toast.LENGTH_LONG).show();
            btnOpenAppUsageSettings.setVisibility(View.VISIBLE);
            btnOpenAppUsageSettings.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                }
            });
        }
        return queryUsageStats;
    }

    /**
     * Updates the {@link #recyclerView} with the list of {@link UsageStats} passed as an argument.
     *
     * @param usageStatsList A list of {@link MyUsageStats} from which update the {@link #recyclerView}.
     */
    private void updateAppsList(List<UsageStats> usageStatsList) {
        List<MyUsageStats> myUsageStatsList = new ArrayList<>(25);
        for (int i = 0; i < usageStatsList.size(); i++) {
            MyUsageStats myUsageStats = new MyUsageStats();
            myUsageStats.usageStats = usageStatsList.get(i);

            // For some reason the query also returns Apps that were never used.
            // As the list is sorted we can stop here.
            if (myUsageStats.usageStats.getLastTimeUsed() == 0) break;

            try {
                myUsageStats.appIcon = getActivity().getPackageManager().getApplicationIcon(myUsageStats.usageStats.getPackageName());
                //ApplicationInfo app = getActivity().getPackageManager().getApplicationInfo(myUsageStats.usageStats.getPackageName(), 0);
                //myUsageStats.appIcon = app.loadIcon(getActivity().getPackageManager());
            } catch (PackageManager.NameNotFoundException | NullPointerException e) {
                //Log.e(TAG, "Foutje:", e);
                Log.w(TAG, String.format("App Icon is not found for %s", myUsageStats.usageStats.getPackageName()));
                myUsageStats.appIcon = getActivity().getDrawable(R.drawable.ic_default_app_launcher);
            }
            myUsageStatsList.add(myUsageStats);
        }
        myAdapter.setCustomUsageStatsList(myUsageStatsList);
        myAdapter.notifyDataSetChanged();
        recyclerView.scrollToPosition(0);
    }

    /**
     * The {@link Comparator} to sort a collection of {@link UsageStats} sorted by the timestamp
     * last time the app was used in the descendant order.
     */
    private static class LastTimeLaunchedComparatorDesc implements Comparator<UsageStats> {

        @Override
        public int compare(UsageStats left, UsageStats right) {
            return Long.compare(right.getLastTimeUsed(), left.getLastTimeUsed());
        }
    }

    /**
     * Enum represents the intervals for {@link android.app.usage.UsageStatsManager} so that
     * values for intervals can be found by a String representation.
     */
    private enum StatsUsageInterval {
        DAILY("Daily", UsageStatsManager.INTERVAL_DAILY),
        WEEKLY("Weekly", UsageStatsManager.INTERVAL_WEEKLY),
        MONTHLY("Monthly", UsageStatsManager.INTERVAL_MONTHLY),
        YEARLY("Yearly", UsageStatsManager.INTERVAL_YEARLY);

        private int interval;
        private String stringRepresentation;

        StatsUsageInterval(String stringRepresentation, int interval) {
            this.stringRepresentation = stringRepresentation;
            this.interval = interval;
        }

        public static StatsUsageInterval getValue(String stringRepresentation) {
            for (StatsUsageInterval statsUsageInterval : values()) {
                if (statsUsageInterval.stringRepresentation.equals(stringRepresentation)) {
                    return statsUsageInterval;
                }
            }
            return null;
        }
    }
}
