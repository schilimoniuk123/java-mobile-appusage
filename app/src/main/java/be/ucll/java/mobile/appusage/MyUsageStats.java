package be.ucll.java.mobile.appusage;

import android.app.usage.UsageStats;
import android.graphics.drawable.Drawable;

/**
 * Entity class represents usage stats and app icon.
 */
public class MyUsageStats {
    public UsageStats usageStats;
    public Drawable appIcon;
}
